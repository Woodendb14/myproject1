from final_project.admin import Admin
from datetime import datetime


class Supervisor(Admin):
    """
    для выполнения системных операций.
    """

    def __init__(self, group, login, password, relevance, account, email):
        super().__init__(group, login, password, relevance, account, email)

        def get_group(self):
            return 'Admins'


        # актуальность сертификатов

    def sert_verify(a):
        a = 'other'
        now = datetime.now()
        dead_sert = datetime(2020, 12, 31)
        if now > dead_sert:
            print("Сертификат недействителен")
        elif now.day == dead_sert.day and now.month == dead_sert.month and now.year == dead_sert.year:
            print("Сертификат заканчивается сегодня")
        else:
            period = dead_sert - now
            print(f"Сертификат истекает через {format(period.days)}д.")


