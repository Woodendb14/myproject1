"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""

def check_substr (a,b):
    if a.find(b) != 0:
        print('True')
    elif b.find(a) != 0:
        print('True')
    elif len(a) == len(b):
        print('False')
    elif len(a) == 0 or len(b) == 0:
        print('True')
    else:
        print('False')

check_substr ('4567', '')
