from random import randint

from final_project.user import User


class VipUser(User):
    """
    VIPUSER
    """

    def __init__(self, name, group, birthday, login, password, account, relevance, email):
        super().__init__(name, group, birthday, login, password, account, relevance, email)
        self.favourite_actions = ['Донатить', "Просматривать контент", "Хэйтить"]

    def get_favourite_action(self):
        num = randint(0, len(self.favourite_actions) - 1)
        return self.favourite_actions[num]

    def get_group(self):
        return 'Users'


if __name__ == '__main__':
    act1 = VipUser('Дима', 'Users', 19810314, 'Dims123', 'pass123!@#', 15211.12, 'act', 'wooden@list.ru')
    print(act1.get_favourite_action())
