from final_project.user import User


class DemoUser(User):
    """
    Неподтвержденный аккаунт (Демо версия)
    """
    def __init__(self, name, group, birthday, login, password, account, relevance, email):
        super().__init__(name, group, birthday, login, password, account, relevance, email)

    def demo_viewing(self):
        return print('Я только просматриваю контент')

    def get_group(self):
        return 'Users'

if __name__ == '__main__':
    act1 = DemoUser('Дима', 'Users', 19810314, 'Dims123', 'pass123!@#', 15211.12, 'act', 'wooden@list.ru')
    print(act1.get_favourite_action())



