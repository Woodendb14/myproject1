from final_project.user import User


class Admin(User):
    """
    Администратор
    """

    def __init__(self, name, group, birthday, login, password, account, relevance, email):
        self.rem_users = {}

        super().__init__(name, group, birthday, login, password, account, relevance, email)



    def add_users(self, user):
        user_group = user.get_group()
        if self.rem_users.get(user_group, True):
            self.rem_users[user_group] = []
        self.rem_users[user_group].append(user)



    def verify_user(self):
        return 'Я проверяю пользователей'

    def disable_user(self):
        return 'Я отключаю пользователей'

    def activate_user(self):
        return 'Я активирую пользователей'

    def get_group(self):
        return 'Admins'

