"""
Функция dict_triple_start_stop_step_if_div5.

Принимает 3 аргумента: число start, stop, step.

Возвращает генератор-выражение состоящий из кортежа (аналог dict):
(значение, утроенное значение)
при этом значения перебираются от start до stop (не включая) с шагом step
только для чисел, которые делятся на 5 без остатка.

Пример: start=0, stop=16, step=1 результат ((0, 0), (5, 15), (10, 30), (15, 45)).
"""