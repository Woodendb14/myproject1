from final_project.demo_user import Demo_user


class Guest(Demo_user):
    """
    Гость деактивируется через 5 минут
    """

    def __init__(self, login, relevance):
        super().__init__(login, relevance)


    def get_group(self):
        return 'Users'


    def demo_viewing(self):
        return 'Я только просматриваю контент)'  # в течении 5 минут далее учетка деактивируется
