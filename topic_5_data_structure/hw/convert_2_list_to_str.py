"""
Функция list_to_str.
Принимает 2 аргумента: список и разделитель (строка).
Возвращает (строку полученную разделением элементов списка разделителем,
            количество разделителей в получившейся строке в квадрате).
Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.
Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.
Если список пуст, то возвращать пустой tuple().
ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
"""
def list_to_str(my_list, my_str):
    if type(my_list) != list:
        return print('First arg must be list!')
    if type(my_str) != str:
        return print("Second arg must be str!")
    if len(my_list) == 0:
        return tuple()

    sp_1 = []

    return print((my_str * 2).join(str(my_list)))



list_to_str(['a', 'b', [1,2], 'd', 'e'], 'T')

