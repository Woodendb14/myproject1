"""
Функция print_hi.

Принимает число n.
Выведите на экран n раз фразу "Hi, friend!"
Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"
"""


def print_hi (n):
    return (print('"'+'Hi, friend!'*n+'"'))
print_hi(int(input("Введите число : ")))