"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""

def print_symbols_if(a: str):
    b = len(a)
    if len(a) == 0:
       return(print("Empty string!"))
    elif len(a) > 5:
        print(len(a))
        print('A = '+a[0:3:1])
        print('B = '+a[-3:])
        return print('ALL = '+ a[:3:] + a[-3::])
    else:
        return print('<3 = ' + a[:1:] *3)




print_symbols_if('Hel')
