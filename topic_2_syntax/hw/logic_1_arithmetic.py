"""
Функция arithmetic.

Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними.
Если третий аргумент +, сложить их;
если —, то вычесть;
если *, то умножить;
если /, то разделить (первое на второе).
В остальных случаях вернуть строку "Unknown operator".
Вернуть результат операции.
"""


def arithmetic(x, y, c):

        if c == '+':
            z = x+y
            return print (f' {x} {c} {y} = {z}')
        elif c == '-':
            z = x-y
            return print  (f' {x} {c} {y} = {z}')
        elif c == '/':
            z = x/y
            return print  (f' {x} {c} {y} = {z}')
        elif c == '*':
            z = x*y
            return print  (f' {x} {c} {y} = {z}')
        elif c == ':':
            z = x / y
            return print(f' {x} {c} {y} = {z}')
        else:
            return print('Unknown operator')

result = arithmetic (int(input("Введите число a: ")), int(input("Введите число b :")), input("Введите операцию: "))

