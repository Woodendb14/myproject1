"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""

def count_odd_num (a):
    even = odd = 0
    if a == 0:
        odd = 'Must be > 0!'
    if type(a) != int:
        print('Must be int! input = '+ str(type(a)))
    while a > 0:
        if a == 0:
            odd = 'Must be > 0!'
        elif a % 2 == 0:
            even += 1
        else:
            odd += 1
        a = a // 10
    return print(odd)
count_odd_num(45252.757)


