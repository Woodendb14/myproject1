"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает (список ключей,
            список значений,
            количество уникальных элементов в списке ключей,
            количество уникальных элементов в списке значений).

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.
"""


def dict_to_list(dict_1: dict):
    if type(dict_1) != dict:
        return 'Must be dict!'
    ex2 = set()
    return print(dict_1.keys(), dict_1.values(), len(set(dict_1)), len(dict_1))


dict_to_list({1: 'a', 2: 'b', 3: 'c'})


